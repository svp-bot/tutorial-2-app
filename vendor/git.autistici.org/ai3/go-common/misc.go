package common

import (
	"crypto/x509"
	"io/ioutil"
)

// LoadCA loads a file containing CA certificates into a x509.CertPool.
func LoadCA(path string) (*x509.CertPool, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	cas := x509.NewCertPool()
	cas.AppendCertsFromPEM(data)
	return cas, nil
}
