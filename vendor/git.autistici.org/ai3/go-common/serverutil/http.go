package serverutil

import (
	"context"
	"crypto/tls"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var gracefulShutdownTimeout = 3 * time.Second

// ServerConfig stores common HTTP/HTTPS server configuration parameters.
type ServerConfig struct {
	TLS                 *TLSServerConfig `yaml:"tls"`
	MaxInflightRequests int              `yaml:"max_inflight_requests"`
}

// Serve HTTP(S) content on the specified address. If serverConfig is
// not nil, enable HTTPS and TLS authentication.
//
// This function will return an error if there are problems creating
// the listener, otherwise it will handle graceful termination on
// SIGINT or SIGTERM and return nil.
func Serve(h http.Handler, serverConfig *ServerConfig, addr string) (err error) {
	var tlsConfig *tls.Config
	if serverConfig != nil {
		if serverConfig.TLS != nil {
			tlsConfig, err = serverConfig.TLS.TLSConfig()
			if err != nil {
				return err
			}
			h, err = serverConfig.TLS.TLSAuthWrapper(h)
			if err != nil {
				return err
			}
		}

		if serverConfig.MaxInflightRequests > 0 {
			h = newLoadSheddingWrapper(serverConfig.MaxInflightRequests, h)
		}
	}

	srv := &http.Server{
		Addr:         addr,
		Handler:      instrumentHandler(h),
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		IdleTimeout:  60 * time.Second,
		TLSConfig:    tlsConfig,
	}

	// Install a signal handler for gentle process termination.
	done := make(chan struct{})
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("exiting")

		// Gracefully terminate for 3 seconds max, then shut
		// down remaining clients.
		ctx, cancel := context.WithTimeout(context.Background(), gracefulShutdownTimeout)
		defer cancel()
		if err := srv.Shutdown(ctx); err == context.Canceled {
			if err := srv.Close(); err != nil {
				log.Printf("error terminating server: %v", err)
			}
		}

		close(done)
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		return err
	}

	<-done
	return nil
}

func instrumentHandler(h http.Handler) http.Handler {
	root := http.NewServeMux()
	root.Handle("/metrics", promhttp.Handler())
	root.Handle("/", h)
	return promhttp.InstrumentHandlerInFlight(inFlightRequests,
		promhttp.InstrumentHandlerCounter(totalRequests, root))
}

// HTTP-related metrics.
var (
	totalRequests = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "total_requests",
			Help: "Total number of requests.",
		},
		[]string{"code"},
	)
	inFlightRequests = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "inflight_requests",
			Help: "Number of in-flight requests.",
		},
	)
)

func init() {
	prometheus.MustRegister(totalRequests, inFlightRequests)
}
