package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"

	"git.autistici.org/ai3/go-common/serverutil"
)

var (
	secretToken string
	defaultAddr = ":7007"
)

// This app does nothing except serving the secret token. Oh no!
func handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	fmt.Fprintf(w, "Hello, the secret is %s\n", secretToken)
}

func main() {
	// Read secret token from the environment, complain if it's
	// missing.
	secretToken = os.Getenv("SECRET_TOKEN")
	if secretToken == "" {
		log.Fatal("The SECRET_TOKEN environment variable is not defined")
	}

	// Read bind address from environment, but with a
	// default. Provide a quick syntax check on the value just to
	// be nicer to the user.
	addr := os.Getenv("ADDR")
	if addr == "" {
		addr = defaultAddr
	}
	if _, _, err := net.SplitHostPort(addr); err != nil {
		log.Fatalf("The ADDR environment variable is not correct (it should be either 'HOST:PORT' or simply ':PORT'): %v", err)
	}

	// Start the server.
	log.Printf("tutorial-2-app starting on %s", addr)
	err := serverutil.Serve(http.HandlerFunc(handler), nil, addr)
	if err != nil {
		log.Fatal(err)
	}
}
